const {getAccess, getVersion} = require("./assets/connectApi.js")
const {setWormHole} = require("./assets/wormHole.js")
const {subCommands, updateClient} = require("./assets/function.js")

//const checkVersion = getVersion()
//if (checkVersion) return console.log(checkVersion);

module.exports = {
    getAccess: getAccess,
	getVersion: getVersion,
    setWormHole: setWormHole,
    subCommands: subCommands,
    updateClient: updateClient
};
