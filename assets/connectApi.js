const XMLHttpRequest = require("../../xmlhttprequest").XMLHttpRequest

module.exports = {
	getAccess,
    getVersion
};

function getAccess(tokenBOT, eventName) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "http://185.44.81.130/android/test.php", false);
    if (!tokenBOT) return console.log("GunibotAPI - missing token !");
    if (!eventName) return console.log("GunibotAPI - missing event !");
    xmlHttp.setRequestHeader("TOKEN", tokenBOT)
    xmlHttp.setRequestHeader("REQUEST", eventName)
    xmlHttp.send();

    if (xmlHttp.responseText === "ERREUR_EVENT") return console.log("GunibotAPI - This event does not exist !");
    if (xmlHttp.responseText === "ERREUR_USE_EVENT") return console.log("GunibotAPI - You cannot use this event !");

    //return xmlHttp.responseText;
}

function getVersion() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "http://185.44.81.130/android/test.php", false);
    xmlHttp.setRequestHeader("VERSION", "GET")
    xmlHttp.send();

    const version = require("../package.json").version;
    const nowVersion = xmlHttp.responseText

    if (version !== nowVersion) return ("[GuniBot Package - Err] Your package is out of date, ordered \"npm update gunibot\" to put the package with the new version " + nowVersion + "!");
    return false;
}